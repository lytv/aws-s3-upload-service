import 'express-async-errors';

import express from 'express';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import helmet from 'helmet';

import { NodeEnv, NODE_ENV, CONTENT_MAX_SIZE, CONTENT_TYPES, CONTENT_EXTENSIONS } from '@shared/config';
import { NotFoundError, ServerError } from './middlewares/errorHadlers';
import router from './router';


const app = express();

// Middlewares
app.use(express.raw({ limit: CONTENT_MAX_SIZE, type: CONTENT_TYPES }));

console.log('Service settings:', { CONTENT_MAX_SIZE, CONTENT_TYPES, CONTENT_EXTENSIONS });

app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

// Show routes called in console during development
if (NODE_ENV === NodeEnv.DEV) {
  app.use(morgan('dev'));
}

// Security
if (NODE_ENV === NodeEnv.PROD) {
  app.use(helmet());
}

// add route
app.use('/', router);

// Error handlers
app.use(NotFoundError);
app.use(ServerError);


export default app;
