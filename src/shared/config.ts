import mime from 'mime-types';

export enum NodeEnv {
  DEV = 'development',
  PROD = 'production',
}

const { env } = process;

// Set defaults, *.env validate, process variables
const {
  CONTENT_EXTENSIONS: CONTENT_EXTENSIONS_STR,
  CONTENT_TYPES: CONTENT_TYPES_STR,
  ALLOW_ALL_EXTENSIONS_OF_TYPES = 'FALSE',
  SIZE_LARGE: SIZE_LARGE_STR = '2048x2048',
  SIZE_MEDIUM: SIZE_MEDIUM_STR = '1024x1024',
  SIZE_THUMB: SIZE_THUMB_STR = '300x300',
} = env;
const contentTypes: string[] = CONTENT_TYPES_STR ? CONTENT_TYPES_STR.split(',') : [];
const contentExtensions: string[] = CONTENT_EXTENSIONS_STR ? CONTENT_EXTENSIONS_STR.split(',') : [];


if (!CONTENT_EXTENSIONS_STR && !CONTENT_TYPES_STR) {
  throw new Error('at least one of CONTENT_EXTENSIONS_STR or CONTENT_EXTENSIONS_STR variables is required');
}

export const PORT: number = Number(env.PORT) || 3000;

export const NODE_ENV: NodeEnv = (env.NODE_ENV as NodeEnv | undefined) || NodeEnv.DEV;

export const CONTENT_MAX_SIZE: string = env.CONTENT_MAX_SIZE || '100kb';

export const CONTENT_TYPES = contentExtensions.reduce(
  (types: string[], ext: string) => {
    const type: string | false = mime.lookup(ext);
    if (!type) {
      throw new Error(`Incorrect content extension ".${ext}"`);
    }
    else if (!types.includes(type)) {
      types.push(type);
    }
    return types;
  },
  contentTypes
);

export const CONTENT_EXTENSIONS: string[] = contentTypes.reduce(
  (extensions: string[], type: string) => {
    const typeExtensions: string[] | false = mime.extensions[type];
    if (!typeExtensions) {
      throw new Error(`Incorrect content type "${type}"`);
    }
    else if (ALLOW_ALL_EXTENSIONS_OF_TYPES === 'TRUE') {
      typeExtensions.forEach(item => {
        if (!extensions.includes(item)) extensions.push(item);
      });
    }
    return extensions;
  },
  contentExtensions
);

export const AWS_ACCESS_KEY_ID = env.AWS_ACCESS_KEY_ID as string;
if (!AWS_ACCESS_KEY_ID) throw new Error('AWS_ACCESS_KEY_ID is required');

export const AWS_SECRET_ACCESS_KEY = env.AWS_SECRET_ACCESS_KEY as string;
if (!AWS_SECRET_ACCESS_KEY) throw new Error('AWS_SECRET_ACCESS_KEY is required');

export const AWS_BUCKET_NAME = env.AWS_BUCKET_NAME as string;
if (!AWS_BUCKET_NAME) throw new Error('AWS_BUCKET_NAME is required');

export const SIZES = {
  large: SIZE_LARGE_STR.split('x').map(item => Number(item)),
  medium: SIZE_MEDIUM_STR.split('x').map(item => Number(item)),
  thumb: SIZE_THUMB_STR.split('x').map(item => Number(item)),
}

