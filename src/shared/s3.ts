import AWS from 'aws-sdk';
import { promisify } from 'util';

import { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_BUCKET_NAME } from '@shared/config';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';

type S3AsyncUpload = (params: AWS.S3.PutObjectRequest) => Promise<ManagedUpload.SendData>;


const s3 = new AWS.S3({ accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_SECRET_ACCESS_KEY });

const s3PromisifiedUpload: S3AsyncUpload = promisify(s3.upload.bind(s3));

export default function s3Upload(name: string, file: Buffer) {
  return s3PromisifiedUpload(
    {
      Bucket: AWS_BUCKET_NAME,
      Key: name,
      Body: file,
      ACL: 'public-read'
    },
  )
}
