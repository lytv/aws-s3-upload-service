import { Request } from 'express';


export interface IRequest extends Request {
  params: {
    name: string
  }
  body: Buffer;
}
