import { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import mime from 'mime-types';

import { CONTENT_TYPES, CONTENT_EXTENSIONS } from '@shared/config';


const { BAD_REQUEST, FORBIDDEN } = StatusCodes;

export default ((req: Request, res: Response, next: NextFunction) => {
  const contentType = req.header('Content-Type');
  const { name } = req.params;
  const splitName = name.split('.');
  const ext = splitName.pop()?.toLowerCase();

  const clientErrors = [];
  if (!req.body?.length) {
    clientErrors.push('Request body is empty');
  }
  if (!contentType) {
    clientErrors.push('Content-Type header is required');
  } else if (!mime.extension(contentType)) {
    clientErrors.push(`Content-Type header '${contentType}' is invalid`);
  }
  if (splitName.length < 1) {
    clientErrors.push('File extension is required');
  } else if (!mime.lookup(ext as string)) {
    clientErrors.push(`File extension '.${ext as string}' is invalid`);
  }
  if (!clientErrors.length && !mime.extensions[contentType as string].includes(ext as string)) {
    clientErrors.push(`Content Type '${contentType as string}' isn't compatible with extension '.${ext as string}'`);
  }
  // TODO: maybe implement content validation (is req.body compatible with contentType and ext)
  if (clientErrors.length) {
    return res.status(BAD_REQUEST).send({ errors: clientErrors });
  }

  const accessErrors = [];
  if (!CONTENT_EXTENSIONS.includes(ext as string)) {
    accessErrors.push(`File extension '.${ext as string}' is forbidden`);
  }
  if (!CONTENT_TYPES.includes(contentType as string)) {
    accessErrors.push(`Content type '${contentType as string}' is forbidden`);
  }
  if (accessErrors.length) {
    return res.status(FORBIDDEN).send({ errors: accessErrors });
  }

  next();
});
