import { NextFunction, Request, Response } from 'express';
import StatusCodes from 'http-status-codes';
import logger from '@shared/Logger';

const { INTERNAL_SERVER_ERROR, NOT_FOUND } = StatusCodes;


export const NotFoundError = ((req: Request, res: Response) => {
  res.status(NOT_FOUND).send({ error: 'Not found' });
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const ServerError = ((err: Error, req: Request, res: Response, next: NextFunction) => {
  logger.err(err, true);

  return res.status(INTERNAL_SERVER_ERROR).json({
    error: err.message,
  });
});
