import { Router } from 'express';
import upload from './handlers/upload';
import validate from './middlewares/validation'


const router = Router();

router.post('/:name', [validate, upload]);

export default router;
