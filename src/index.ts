import './pre-start';

import logger from '@shared/Logger';
import { PORT } from '@shared/config';

import app from './Server';


app.listen(PORT, () => {
  logger.info('Express server started on port: ' + PORT);
});
