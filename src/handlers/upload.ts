import { Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import sharp from 'sharp';

import s3Upload from '@shared/s3';
import { SIZES } from '@shared/config';
import { IRequest } from '@shared/types';
import { imageTypesToCrop } from '@shared/constants';

const { CREATED } = StatusCodes;


export default async function upload(req: IRequest, res: Response) {
  const { name } = req.params;

  const splitName = name.split('.');
  const ext = splitName.pop()?.toLowerCase() as string;
  const nameWithoutExt = splitName.join();

  const sharpUploadPromises: Promise<string>[] = [];

  // TODO: optimize memory usage

  if (imageTypesToCrop.includes(ext)) {
    Object.keys(SIZES).forEach(key => {
      // TODO: maybe handle small images, if less then SIZE, not crop

      const [width, height] = SIZES[key as keyof typeof SIZES];

      sharpUploadPromises.push(
        (async () => {
          const resizedBuffer = await sharp(req.body).resize(width, height).toBuffer();
          const uploadResult = await s3Upload(`images/${nameWithoutExt}-${key}.${ext}`, resizedBuffer);

          return uploadResult.Location;
        })()
      );
    });
  }
  else {
    sharpUploadPromises.push(
      (async () => {
        const uploadResult = await s3Upload(`files/${name}`, req.body);
        return uploadResult.Location;
      })()
    );
  }

  const results = await Promise.all(sharpUploadPromises);

  res.status(CREATED).send({ status: 'OK', addedFilesPaths: results })
}
